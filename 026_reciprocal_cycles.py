import time

if __name__ == '__main__':
    max_count = 0
    for i in range(1, 1001, 2):
        cycles = []
        tmp = 10
        cycles.append(1)
        while True:
            remainder = tmp % i
            if (remainder in cycles) or (remainder == 0):
                if len(cycles) > max_count:
                    print(i, len(cycles))
                    max_count = len(cycles)
                break
            else:
                cycles.append(remainder)
                tmp = remainder * 10
