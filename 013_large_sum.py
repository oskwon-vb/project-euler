"""
# 013_large_sum.py
# Date: 2014. 08. 12
# Author: coozplz@gmail.com
# 학습내용
    1. 파일 읽기 (라인 구분자 제거)
    2. String to int
    3. int to str
"""
lines = (line.rstrip("\n") for line in open("./resource/largesum.txt"))
sumResult = 0
for line in lines:
    sumResult += int(line)
print(str(sumResult)[0:10])