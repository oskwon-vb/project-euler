"""
# 004_palindrome_product.py
# author: coozplz
# date: 2014. 08. 07
# problem: 2자리 계산으로 구할수 있는 회문(palindrome)은 9009 이다.
           그럼 3자리 곱셈으로 구할 수 있는 가장 큰 회문은 몇인가
"""


def is_palindrome(num):
    str_num = str(num)
    s_index = str_num[:int(len(str_num)/2)]
    e_index = str_num[int(len(str_num)/2):]
    print(str_num, s_index, e_index)
    print(str_num, s_index, "".join(reversed(e_index)))
    if s_index == "".join(reversed(e_index)):
        print("same")
    # print(str_num[len(str_num)/2:], str_num[:len(str_num)/1], len(str_num))



is_palindrome(123321)