import math
import unittest


class TestPythagorean(unittest.TestCase):

    def testPythagorean(self):
        for i in range(1000, 0, -1):
            for j in range(1, i):
                c = math.sqrt((i*i) + (j*j))
                c1, c2 = math.modf(c)

                if c1 != 0:
                    continue

                if c2 < i:
                    continue

                if self.isPythagorean(j, i, c) and i+j+c == 1000:
                    print("Pythagorean = ", j*i*c)

    def isPythagorean(self, a, b, c):
        aa = a * a
        bb = b * b
        cc = c * c
        # print("%d + %d = %d" % (aa, bb, cc))
        if aa + bb == cc:
            return True
        return False


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPythagorean)
    unittest.TextTestRunner(verbosity=2).run(suite)